# Get base image of alpine-node

# Install phantomjs
#COPY phantomjs.sh .
#RUN ./phantomjs.sh
FROM node:7.10.0-alpine

# Install dependencies, then delete the cache
RUN apk --update add --no-cache openssh bash curl \
  && sed -i s/#PermitRootLogin.*/PermitRootLogin\ yes/ /etc/ssh/sshd_config \
  && echo "root:root" | chpasswd \
  && rm -rf /var/cache/apk/*
# Install dependencies, replaced by .sh file later
RUN sed -ie 's/#Port 22/Port 22/g' /etc/ssh/sshd_config
RUN sed -ri 's/#HostKey \/etc\/ssh\/ssh_host_key/HostKey \/etc\/ssh\/ssh_host_key/g' /etc/ssh/sshd_config
RUN sed -ir 's/#HostKey \/etc\/ssh\/ssh_host_rsa_key/HostKey \/etc\/ssh\/ssh_host_rsa_key/g' /etc/ssh/sshd_config
RUN sed -ir 's/#HostKey \/etc\/ssh\/ssh_host_dsa_key/HostKey \/etc\/ssh\/ssh_host_dsa_key/g' /etc/ssh/sshd_config
RUN sed -ir 's/#HostKey \/etc\/ssh\/ssh_host_ecdsa_key/HostKey \/etc\/ssh\/ssh_host_ecdsa_key/g' /etc/ssh/sshd_config
RUN sed -ir 's/#HostKey \/etc\/ssh\/ssh_host_ed25519_key/HostKey \/etc\/ssh\/ssh_host_ed25519_key/g' /etc/ssh/sshd_config
RUN /usr/bin/ssh-keygen -A
RUN ssh-keygen -t rsa -b 4096 -f  /etc/ssh/ssh_host_key

# Install dependencies for
RUN yarn global add pm2@2.2.3
RUN pm2 install pm2-logrotate
# Install meteor
RUN curl https://install.meteor.com/ | sh

EXPOSE 22 80 443
#CMD ["/etc/init.d/sshd start"]
MAINTAINER vuonghoainam <vuonghoainam@gmail.com>
ENTRYPOINT ["/usr/sbin/sshd","-D"]
