#!/bin/bash
function waitForSSH()
{
	service ssh start
    echo Waiting for SSH to come up...
    while ! service ssh status
	do
		sleep 0.5
	    echo "Trying again..."
	done
	cd /root
	export HOME="/root"
	export METEOR_ALLOW_SUPERUSER=true
	ln -s /usr/bin/python2.7 /usr/bin/python

	echo pm2-meteor is deploying...
	# pm2-meteor deploy &> /root/logs.txt
	pm2-meteor deploy
	# remove meteor & remove deploy data
	rm -rf /root/.meteor
	# rm -rf /opt/pm2-meteor
    exit 0
}
#You must background the function call or the image boot will be blocked!
waitForSSH 
